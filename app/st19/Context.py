from .Worker import Worker
from flask import render_template,request
from abc import ABC, abstractmethod
from .Chief import Chief
from .Engeneer import Engeneer
from .Specialist import Specialist
from .ChiefSpecialist import ChiefSpecialist

from dataclasses import dataclass

_list = {
        'Начальник':Chief,
        'Инженер':Engeneer,
        'Специалист':Specialist,
        'Главный специалист':ChiefSpecialist
        }
        
@dataclass
class GroupItem:
    id: int = -1
    _first_name: str = ''
    _second_name: str = ''
    _gender: str = ''
    _age: int = ''
    _magic: int = 0
    _position: str = 'Инженер'
    _worker = _list.get(_position)
    

    def do_magic_logic(self):
        return(self._worker().do_magic(self._magic))
        
    def Show(self):
        return "BootItem Edit"

    def DBLoad(self, r):
        self.id = r['id']
        self._worker = _list.get(r['position'])
        self._position = r['position']
        self._first_name = r['first_name']
        self._second_name = r['second_name']
        self._gender =r['gender']
        self._age = int(r['age'])
        self._magic = int(r['magic'])

        # self._first_name = r[1] if r else ''
        # self._second_name = r[2] if r else ''
        # self._gender = r[3] if r else ''
        # self._age = int(r[4]) if r else 0
        # self._magic = int(r[5]) if r else 0

        # self.id = r['id']
        # self.title = r['title']
        # self.body = r['body']
        # self.author = r['author']
        # self.time = r['time']
        
    def DBStore(self, db):
        if not self.id or int(self.id) < 0:
            db.execute("INSERT INTO workers VALUES(NULL, ?, ?, ?, ?, ?, ?)", (self._first_name, self._second_name, self._gender, self._age, self._magic, self._position,))
        else:
            db.execute("update workers set first_name=?, second_name=?, gender=?, age=?, magic=?, position=? where id=?", (self._first_name, self._second_name, self._gender, self._age, self._magic, self._position, self.id,))
        
            

    def Input(self, io): #do_add_worker
        self._first_name = io.Input('_first_name')
        self._second_name = io.Input('_second_name')
        self._gender = io.Input('_gender')
        self._age = int(io.Input('_age'))
        self._magic = int(io.Input('_magic'))
        self._position = io.Input('_position')

    def Output(self, io):
        return io.Output(self.__dict__)




# class ContextMain(ABC):
#     ___dict__ = ('_first_name', '_second_name', '_gender', '_age','_magic','_position')
#     def __init__(self):
#         self._position = ''
#         self._first_name = ''
#         self._second_name = ''
#         self._gender = ''
#         self._age = 0
#         self._magic = 0

#     def do_magic_logic(self):
#         return(self._worker.do_magic(self._magic))
#     @abstractmethod
#     def do_add_worker(self):
#         pass
    
#     def dbinit(self, worker: Worker, it = None):
#         self._worker = worker
#         self._position = self._worker.do_print()[0]
#         self._first_name = it[1] if it is not None else ''
#         self._second_name = it[2] if it is not None else ''
#         self._gender = it[3] if it is not None else ''
#         self._age = int(it[4]) if it is not None else 0
#         self._magic = int(it[5]) if it is not None else 0

# class Context(ContextMain):
#     def do_add_worker(self):
#         self._first_name = request.form.get('_first_name')
#         self._second_name = request.form.get('_second_name')
#         self._gender = request.form.get('_gender')
#         self._age = int(request.form.get('_age'))
#         self._magic = int(request.form.get('_magic'))

    
# class ContextApi(ContextMain):
#     def do_add_worker(self):
#         self._first_name = request.json['_first_name']
#         self._second_name = request.json['_second_name']
#         self._gender = request.json['_gender']
#         self._age = int(request.json['_age'])
#         self._magic = int(request.json['_magic'])

class ConsoleInputOutput:
    def Input(self, field, defval=None):
        return input(field)

    def Output(self, item):
        print(item) 

    def __str__(self):
        return ''
