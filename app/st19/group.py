﻿import pickle
from flask import render_template,request,g,redirect,jsonify,abort
import os
from .Context import GroupItem

import sqlite3

selfpath = 'data/st19/'
templpath = 'st19/'

class Group:
    def __init__(self):
        self.storage = DBStorage(self)
        self.io = FlaskInputOutput(request.form)
        self.restio = RESTInputOutput(request.json)

    def do_magic(self,id):
        item = self.storage.GetItem(id)
        ret = 'ID = '+str(id)+' This is Magic!<br>'
        ret += item.do_magic_logic()
        return render_template(templpath + "magic.tpl",magic=ret,pos=item._worker().do_print()[1],selfurl=g._selfurl)


    def change_worker(self, id):
        return self.storage.GetItem(id).Output(self.io)


    def add_worker(self):
        item = self.storage.GetItem(int(request.form.get('id', -1)))
        item.Input(self.io)
        self.storage.Add(item)
        return redirect(f'{g._selfurl}/')
   
    def print_group(self):
        return render_template(templpath + "item.tpl",workers=self.storage.GetItems(),selfurl=g._selfurl)

    def remove_worker(self,id):
        self.storage.Delete(id)
        return redirect(f'{g._selfurl}/')

    def clear_worker(self):
        return redirect(f'{g._selfurl}/')

    def APIGet(self, id = None, status_code = 200):
        if id:
            item = self.storage.GetItem(id)
            ret = {'message':item.do_magic_logic()}
            ret.update({key:str(it) for key,it in item.__dict__.items()  if key !='_worker'})
            return jsonify(ret), status_code
        else:
            return jsonify({item.id : {key:str(it) for key,it in item.__dict__.items()  if key !='_worker'} for item in self.storage.GetItems()}), status_code
            

    def APIAdd(self):
        item = self.storage.GetItem(-1)
        item.Input(self.restio)
        self.storage.Add(item)
        return self.APIGet(status_code = 201)

    def APISet(self, id):
        item = self.storage.GetItem(id)
        item.Input(self.restio)
        self.storage.Add(item)
        return self.APIGet(status_code = 201)
        

    def APIDelete(self, id):
        self.storage.Delete(id)
        return self.APIGet(status_code = 204)


class PickleStorage:
    def __init__ (self, group):
        self.group = group
        try:
            self.Load()
        except:
            self.workers = []

    def Load(self):
        if not os.path.exists(selfpath):
            os.mkdir(selfpath)
        with open(selfpath+'group.db', 'rb') as f:
            self._workers = pickle.load(f)
        
    def Store(self):
        if not os.path.exists(selfpath):
            os.mkdir(selfpath)
        with open(selfpath+'group.db', 'wb') as f:
            pickle.dump(self._workers, f)

    def GetItem(self, id):
        if id <= 0:
            return GroupItem()
        else:
            return self._workers[id]

    def Add(self, item):
        if item.id <= 0:
            self._workers.add(item)
            
    def Delete(self, id):
        del self._workers[id]
        
    def GetItems(self):
        for item in self._workers:
            yield(item)


class DBStorage:
    def __init__ (self, book):
        self.Load()
        
    def Load(self):
        if not os.path.exists(selfpath):
            os.mkdir(selfpath)
        self.db = sqlite3.connect(selfpath+'group_works.sqlite', detect_types=sqlite3.PARSE_DECLTYPES)
        self.db.execute("""
                    CREATE TABLE IF NOT EXISTS workers(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    first_name TEXT,
                    second_name TEXT,
                    gender TEXT,
                    age INTEGER,
                    magic INTEGER,
                    position TEXT
                        )""")
        self.db.row_factory = sqlite3.Row
        self.dbc = self.db.cursor()

        
    def Store(self):
        self.db.commit()
        self.db.close()

    def GetItem(self, id):
        item = GroupItem()
        if id >= 0:
            self.dbc.execute("select * from workers where id=?", (id,))
            item.DBLoad(self.dbc.fetchone())
        return item

    def Add(self, item):
        item.DBStore(self.db)
            
    def Delete(self, id):
        self.db.execute("delete from workers where id=?", (id,))
        
    def GetItems(self):
        self.dbc.execute("SELECT * FROM workers order by id desc")
        for r in self.dbc:
            item = GroupItem()
            item.DBLoad(r)
            yield(item)

class RESTInputOutput:
    def __init__(self, io):
        self.io = io

    def Input(self, field, defval=None):
        return self.io.get(field, defval)

    def Output(self, item):
        print(item) 


class FlaskInputOutput:
    def __init__(self, io):
        self.io = io

    def Input(self, field, defval=None):
        return self.io.get(field, defval)

    def Output(self, item):
        return render_template(templpath+'/form.tpl', work=item, selfurl='/'+request.url_rule.rule.split('/')[1])
