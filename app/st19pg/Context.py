from .Worker import Worker
from flask import render_template,request
from abc import ABC, abstractmethod
from .Chief import Chief
from .Engeneer import Engeneer
from .Specialist import Specialist
from .ChiefSpecialist import ChiefSpecialist

from dataclasses import dataclass

_list = {
        'Начальник':Chief,
        'Инженер':Engeneer,
        'Специалист':Specialist,
        'Главный специалист':ChiefSpecialist
        }
        
@dataclass
class GroupItem:
    id: int = -1
    _first_name: str = ''
    _second_name: str = ''
    _gender: str = ''
    _age: int = ''
    _magic: int = 0
    _position: str = 'Инженер'
    _worker = _list.get(_position)
    

    def do_magic_logic(self):
        return(self._worker().do_magic(self._magic))
        
    def Show(self):
        return "BootItem Edit"

    def DBLoad(self, r):
        self.id = r['id']
        self._worker = _list.get(r['position'])
        self._position = r['position']
        self._first_name = r['first_name']
        self._second_name = r['second_name']
        self._gender =r['gender']
        self._age = int(r['age'])
        self._magic = int(r['magic'])
        
    def DBStore(self, db):
        if not self.id or int(self.id) < 0:
            db.execute("INSERT INTO workers VALUES(DEFAULT,%s, %s, %s, %s, %s, %s);", (self._first_name, self._second_name, self._gender, self._age, self._magic, self._position,))
        else:
            db.execute("update workers set first_name=%s, second_name=%s, gender=%s, age=%s, magic=%s, position=%s where id=%s ;", (self._first_name, self._second_name, self._gender, self._age, self._magic, self._position, self.id,))
        
            

    def Input(self, io): 
        self._first_name = io.Input('_first_name')
        self._second_name = io.Input('_second_name')
        self._gender = io.Input('_gender')
        self._age = int(io.Input('_age'))
        self._magic = int(io.Input('_magic'))
        self._position = io.Input('_position')

    def Output(self, io):
        return io.Output(self.__dict__)