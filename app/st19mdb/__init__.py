from flask import Blueprint,g,request

bp = Blueprint('st19mdb', __name__)

from .group import Group



def GetGroup():
        if 'cur_group' not in g:
                g.cur_group = Group()
        return g.cur_group

@bp.before_request
def before_request():
    g._selfurl = f'/{request.url_rule.rule.split("/")[1]}'

@bp.route("/")
def index():
	return GetGroup().print_group()

@bp.route("/ShowForm/<id>")
def ShowForm(id):
	return GetGroup().change_worker(id)

@bp.route("/DeleteItem/<id>")
def DeleteItem(id):
	return GetGroup().remove_worker(id)

@bp.route("/ShowMagicForm/<id>")
def ShowMagicForm(id):
	return GetGroup().do_magic(id)

@bp.route("/AddItem", methods=['POST'])
def AddItem():
	return GetGroup().add_worker()

@bp.teardown_request
def finish(ctx):
    GetGroup().storage.Store()

@bp.route("/api/", methods=['GET'])
def apibook():
    return GetGroup().APIGet()

@bp.route("/api/<id>", methods=['GET'])
def apiget(id):
    return GetGroup().APIGet(id)

@bp.route("/api/", methods=['POST'])
def apiadd():
    return GetGroup().APIAdd()

@bp.route("/api/<id>", methods=['PUT'])
def apiset(id):
    return GetGroup().APISet(id)

@bp.route("/api/<id>", methods=['DELETE'])
def apidelete(id):
    return GetGroup().APIDelete(id)


