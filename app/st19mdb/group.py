﻿import pickle
from flask import render_template,request,g,redirect,jsonify,abort
import os
from .Context import GroupItem

import sqlite3
import psycopg2
import psycopg2.extras
import pymongo
from bson.objectid import ObjectId

selfpath = 'data/st19/'
templpath = 'st19/'

class Group:
    def __init__(self):
        self.storage = MongoStorage(self)
        self.io = FlaskInputOutput(request.form)
        self.restio = RESTInputOutput(request.json)

    def do_magic(self,id):
        item = self.storage.GetItem(id)
        ret = 'ID = '+str(id)+' This is Magic!<br>'
        ret += item.do_magic_logic()
        return render_template(templpath + "magic.tpl",magic=ret,pos=item._worker().do_print()[1],selfurl=g._selfurl)

    def change_worker(self, id):
        return self.storage.GetItem(id).Output(self.io)


    def add_worker(self):
        item = self.storage.GetItem(request.form.get('id', -1))
        item.Input(self.io)
        self.storage.Add(item)
        return redirect(f'{g._selfurl}/')
   
    def print_group(self):
        return render_template(templpath + "item.tpl",workers=self.storage.GetItems(),selfurl=g._selfurl)

    def remove_worker(self,id):
        self.storage.Delete(id)
        return redirect(f'{g._selfurl}/')

    def clear_worker(self):
        return redirect(f'{g._selfurl}/')

    def APIGet(self, id = None, status_code = 200):
        if id:
            item = self.storage.GetItem(id)
            ret = {'message':item.do_magic_logic()}
            ret.update({key:str(it) for key,it in item.__dict__.items()  if key !='_worker'})
            return jsonify(ret), status_code
        else:
            return jsonify({str(item.id) : {key:str(it) for key,it in item.__dict__.items()  if key !='_worker'} for item in self.storage.GetItems()}), status_code
            

    def APIAdd(self):
        item = self.storage.GetItem(-1)
        item.Input(self.restio)
        self.storage.Add(item)
        return self.APIGet(status_code = 201)

    def APISet(self, id):
        item = self.storage.GetItem(id)
        item.Input(self.restio)
        self.storage.Add(item)
        return self.APIGet(status_code = 201)
        

    def APIDelete(self, id):
        self.storage.Delete(id)
        return self.APIGet(status_code = 204)


class PickleStorage:
    def __init__ (self, group):
        self.group = group
        try:
            self.Load()
        except:
            self.workers = []

    def Load(self):
        if not os.path.exists(selfpath):
            os.mkdir(selfpath)
        with open(selfpath+'group.db', 'rb') as f:
            self._workers = pickle.load(f)
        
    def Store(self):
        if not os.path.exists(selfpath):
            os.mkdir(selfpath)
        with open(selfpath+'group.db', 'wb') as f:
            pickle.dump(self._workers, f)

    def GetItem(self, id):
        if id <= 0:
            return GroupItem()
        else:
            return self._workers[id]

    def Add(self, item):
        if item.id <= 0:
            self._workers.add(item)
            
    def Delete(self, id):
        del self._workers[id]
        
    def GetItems(self):
        for item in self._workers:
            yield(item)


class DBStorage:
    def __init__ (self, worker):
        self.Load()
        
    def Load(self):
        if not os.path.exists(selfpath):
            os.mkdir(selfpath)
        self.db = sqlite3.connect(selfpath+'group_works.sqlite', detect_types=sqlite3.PARSE_DECLTYPES)
        self.db.execute("""
                    CREATE TABLE IF NOT EXISTS workers(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    first_name TEXT,
                    second_name TEXT,
                    gender TEXT,
                    age INTEGER,
                    magic INTEGER,
                    position TEXT
                        );""")
        self.db.row_factory = sqlite3.Row
        self.dbc = self.db.cursor()

        
    def Store(self):
        self.db.commit()
        self.db.close()

    def GetItem(self, id):
        item = GroupItem()
        if id >= 0:
            self.dbc.execute("select * from workers where id=%s ;", (id,))
            item.DBLoad(self.dbc.fetchone())
        return item

    def Add(self, item):
        item.DBStore(self.dbc)
            
    def Delete(self, id):
        self.dbc.execute("delete from workers where id=%s ;", (id,))
        
    def GetItems(self):
        self.dbc.execute("SELECT * FROM workers order by id desc;")
        for r in self.dbc:
            item = GroupItem()
            item.DBLoad(r)
            yield(item)

class PGStorage(DBStorage):
    def Load(self):
        self.db = psycopg2.connect(user=os.getenv('DB_USER', 'postgres'),
                                    password=os.getenv('DB_PASSWORD', 'qwerty'),
                                    host=os.getenv('DB_HOST', 'localhost'),
                                    port=os.getenv('DB_PORT', '5432'),
                                    database=os.getenv('DB_NAME', 'workers'))
        self.dbc = self.db.cursor(cursor_factory=psycopg2.extras.DictCursor)
        self.dbc.execute("""
                    CREATE TABLE IF NOT EXISTS workers(
                    id SERIAL PRIMARY KEY,
                    first_name TEXT,
                    second_name TEXT,
                    gender TEXT,
                    age INTEGER,
                    magic INTEGER,
                    position TEXT);
                    """)
    
    def Store(self):
        self.db.commit()
        self.dbc.close()
        self.db.close()


class MongoStorage:
    def __init__ (self, worker):
        self.Load()
        
    def Load(self):
        if os.getenv('MDB_CONNECT'):
            self.client = pymongo.MongoClient(os.getenv('MDB_CONNECT'))
        else:
            self.client = pymongo.MongoClient(username=os.getenv('MDB_USER', 'mongo'),
                                            password=os.getenv('DB_PASSWORD', 'qwerty'),
                                            host=os.getenv('DB_HOST', 'localhost'),
                                            port=int(os.getenv('MDB_PORT', '27017')))
        self.db = self.client["workers"]
        self.dbc = self.db["workers"]
        
    def Store(self):
        self.client.close()

    def GetItem(self, id):
        item = GroupItem()
        if '-' not in str(id):
            item.DBLoad(self.dbc.find_one({"_id": ObjectId(id)}))
        return item

    def Add(self, item):
        item.DBStore(self)
            
    def Delete(self, id):
        self.dbc.delete_one({"_id": ObjectId(id)})
        
    def GetItems(self):
        for r in self.dbc.find():
            item = GroupItem()
            item.DBLoad(r)
            yield(item)

class RESTInputOutput:
    def __init__(self, io):
        self.io = io

    def Input(self, field, defval=None):
        return self.io.get(field, defval)

    def Output(self, item):
        print(item) 


class FlaskInputOutput:
    def __init__(self, io):
        self.io = io

    def Input(self, field, defval=None):
        return self.io.get(field, defval)

    def Output(self, item):
        return render_template(templpath+'/form.tpl', work=item, selfurl='/'+request.url_rule.rule.split('/')[1])
