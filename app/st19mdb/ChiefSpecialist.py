from .Worker import Worker
from random import randint


class ChiefSpecialist(Worker):
    def do_print(self):
            return('Главный специалист','https://hsto.org/getpro/moikrug/uploads/redactor_image/26012019/images/cc03f7e73e9368a47b8e99984c41c80b.jpg')
    

    def do_magic(self,magic = None):
        rand = randint(0, 210)
        self._share = rand
        ret=''
        if rand == 0:
            ret += 'Вас поймал шеф, придется залечь на дно<br>'
        else:
            ret += 'Удачная сделка с афганскими ребятами<br>'
            self._share += 100
        ret += 'Откат составляет {0}% от месячного дохода'.format(self._share)
        return(ret)

   
