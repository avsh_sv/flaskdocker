from .Worker import Worker
from flask import render_template,request
from abc import ABC, abstractmethod
from .Chief import Chief
from .Engeneer import Engeneer
from .Specialist import Specialist
from .ChiefSpecialist import ChiefSpecialist
from bson.objectid import ObjectId
from dataclasses import dataclass

_list = {
        'Начальник':Chief,
        'Инженер':Engeneer,
        'Специалист':Specialist,
        'Главный специалист':ChiefSpecialist
        }
        
@dataclass
class GroupItem:
    id: int = -1
    _first_name: str = ''
    _second_name: str = ''
    _gender: str = ''
    _age: int = ''
    _magic: int = 0
    _position: str = 'Инженер'
    _worker = _list.get(_position)
    

    def do_magic_logic(self):
        return(self._worker().do_magic(self._magic))
        
    def Show(self):
        return "BootItem Edit"

    def DBLoad(self, r):
        self.id = r['_id']
        self._worker = _list.get(r['_position'])
        self._position = r['_position']
        self._first_name = r['_first_name']
        self._second_name = r['_second_name']
        self._gender =r['_gender']
        self._age = int(r['_age'])
        self._magic = int(r['_magic'])
        
    def DBStore(self, ds):
        if '_worker' in self.__dict__.keys():
            del self.__dict__['_worker']
        if not self.id or '-' in str(self.id):
            if 'id' in self.__dict__.keys():
                del self.__dict__['id']
            ds.dbc.insert_one(self.__dict__)
        else:
            self._id = self.id
            del self.__dict__['id']
            ds.dbc.update_one({"_id": ObjectId(self._id)}, {"$set": self.__dict__})
            

    def Input(self, io): 
        self._first_name = io.Input('_first_name')
        self._second_name = io.Input('_second_name')
        self._gender = io.Input('_gender')
        self._age = int(io.Input('_age'))
        self._magic = int(io.Input('_magic'))
        self._position = io.Input('_position')

    def Output(self, io):
        return io.Output(self.__dict__)