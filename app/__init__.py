from flask import Flask
from flask import render_template
from flask import jsonify

app = Flask(__name__)

#	добавить импорт своего модуля по шаблону

from app.st19 import bp as bp0519
from app.st19pg import bp as bp0519pg
from app.st19mdb import bp as bp0519mdb

#		добавить пункт меню для вызова своего модуля по шаблону:


bps = [
	["Шишкины 1905 SQLite", bp0519],
	["Шишкины 1905 PostgreSQL", bp0519pg],
	["Шишкины 1905 MongoDB", bp0519mdb],
]

for i, (title, bp) in enumerate(bps, start=1):
	app.register_blueprint(bp, url_prefix=f"/st{i}")


@app.route("/")
def index():
	return render_template("index.tpl", bps=bps)

@app.route("/api/", methods=['GET'])
def api():
	sts = []
	for i, (title, bp) in enumerate(bps, start=1):
		sts.append([i, title])
	return jsonify({'sts': sts})
