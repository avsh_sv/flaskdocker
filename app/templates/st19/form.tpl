{% extends 'st19/base.tpl' %}

  {% block content %}

  <div>
    {% if data is defined %}
    <h3>Редактирование сотрудника</h3>  
    {% else %}
    <h3>Добавление сотрудника</h3>
    {% endif %}
  </div>
<form action="{{ selfurl }}/AddItem" method="POST" align="center">
  <input type="hidden" name="id" value={{ work['id'] }}>
  <label>Должность</label><br>
  <select name = "_position">
    <option hidden value = "{{ work['_position'] }}">{{ work['_position'] }}</option>
    <option value = "Начальник">Начальник</option>
    <option value = "Инженер">Инженер</option>
    <option value = "Специалист">Специалист</option>
    <option value = "Главный специалист">Главный специалист</option>
  </select><br>
  <label>Фамилия</label><br>
	<input type=text name=_second_name value="{{work['_second_name']}}"><br>
	<label>Имя</label><br>
	<input type=text name=_first_name value="{{work['_first_name']}}"><br>
	<label>Пол</label><br>
	<input type=text name=_gender value="{{work['_gender']}}"><br>
	<label>Возраст</label><br>
	<input type=number name=_age value="{{work['_age']}}"><br>
	<label>Магическое число</label><br>
	<input type=number name=_magic value="{{work['_magic']}}"><br>
	<input type=submit value=" Ok "><br>
    <a href="{{ selfurl }}/">Отмена</a>
  </form>

  {% endblock %}
