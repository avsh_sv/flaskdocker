 <!DOCTYPE HTML>
 <html>
   <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Лабораторная работа Docker</title>
 <style>
   table { 
    width: 100%; /* Ширина таблицы */
    border: 4px double black; /* Рамка вокруг таблицы */
    border-collapse: collapse; /* Отображать только одинарные линии */
   }
   th { 
    text-align: center; /* Выравнивание по левому краю */
    background: #bbb; /* Цвет фона ячеек */
    padding: 5px; /* Поля вокруг содержимого ячеек */
    border: 1px solid black; /* Граница вокруг ячеек */
   }
   td { 
   	text-align: center;

    padding: 5px; /* Поля вокруг содержимого ячеек */
    border: 1px solid black; /* Граница вокруг ячеек */
   }
   img {
   	opacity: 0;
   }
   img:hover {
   	opacity: .1;
   }
   img:active {
   	opacity: 3;
   }
  </style>
 </head>
  <body>
    <div>
      {% block content %}
      {% endblock %}
    </div>
  </body>
</html>
