{% extends 'st19/base.tpl' %}

      {% block content %}
       <div>
      {% if workers == [] %}
         <h3>Базёнка чиста</h3>
         {%else%}
        <h3>ИТ компания</h3>
      </div>
        <table border = "2">
          <tr>
            <th>ID</th><th>Фамилия</th><th>Имя</th><th>Пол</th><th>Возраст</th><th>Должность</th><th>Действия</th>
          </tr>
          {% for item in workers %}
          <tr>
            <td>{{ item['id'] }}</td><td>{{ item['_second_name'] }}</td><td>{{ item['_first_name'] }}</td><td>{{ item['_gender'] }}</td><td>{{ item['_age'] }}</td><td>{{ item['_position'] }}</td>
            <td>
              <div>
              	<select name = "forma" onchange="location=this.value;">
	        	  <option selected disabled hidden >Выберите действие</option>
				  <option value = "{{ selfurl }}/DeleteItem/{{item['id']}}">Удалить</option>
				  <option value = "{{ selfurl }}/ShowForm/{{item['id']}}">Изменить</option>
				  <option value = "{{ selfurl }}/ShowMagicForm/{{item['id']}}">Выполнить магическое действие</option>
				</select>
              </div>
            </td>
          </tr>
          {% endfor %}
        </table><br>
      {% endif %}
        <div>
        <form><button formaction="{{ selfurl }}/ShowForm/-1">Добавить сотрудника</button></form>
			<form> <!--<button formaction="{{ selfurl }}/DeleteGroup">Сократить всех!</button>--> <button formaction="/">Меню</button></form>
        </div>
      {% endblock %}
